﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BadParallelList
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch sw = new Stopwatch();
            List<int> myInts = new List<int>();

            sw.Start();

            ParallelOptions po = new ParallelOptions();
            po.MaxDegreeOfParallelism = 1;

            Parallel.For(0, 500000, po, (i) =>
            { 
                myInts.Add(i * i);
            });

            sw.Stop();

            Console.WriteLine("Generated: " + myInts.Count + " squared ints");
            Console.WriteLine("in " + sw.ElapsedMilliseconds + "ms");
            Console.ReadKey();
        }
    }
}
